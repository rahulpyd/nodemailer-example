var express=require('express');
var nodemailer = require("nodemailer");
var app=express();
/*
    Here we are configuring our SMTP Server details.
    STMP is mail server which is responsible for sending and recieving email.
*/
// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport('smtps://username%40gmail.com:pass@smtp.gmail.com');
/*------------------SMTP Over-----------------------------*/

/*------------------Routing Started ------------------------*/

app.get('/',function(req,res){
    res.sendFile('index.html', { root: __dirname });
});
app.get('/send',function(req,res){
    var mailOptions={
        to : req.query.to,
        subject : req.query.subject,
        text : req.query.text
    }
    console.log(mailOptions);
    // send mail with defined transport object
  transporter.sendMail(mailOptions, function(error, info){
      if(error){
          res.end("error");
          return console.log(error);
      }
      console.log('Message sent: ' + info.response);
      res.end("sent");
  });
});

/*--------------------Routing Over----------------------------*/

app.listen(3000,function(){
    console.log("Express Started on Port 3000");
});
